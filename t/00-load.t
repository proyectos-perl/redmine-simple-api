#!perl -T

use Test::More tests => 3;

BEGIN {
    use_ok( 'Redmine::Simple::API' ) || print "Bail out!
";
}

diag( "Testing Redmine::Simple::API $Redmine::Simple::API::VERSION, Perl $], $^X" );

my $redmine = Redmine::Simple::API->new(url => 'http://localhost/redmine', apikey =>
    'xx');
isa_ok($redmine, Redmine::Simple::API);

my $redmine = Redmine::Simple::API->new(apikey => 'xx');
isa_ok($redmine, Redmine::Simple::API);

#fail(Redmine::Simple::API->new());
