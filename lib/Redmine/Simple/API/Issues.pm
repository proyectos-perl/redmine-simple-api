package Redmine::Simple::API:Issues;

use warnings;
use strict;
use Moose;
use namespace::autoclean;
use LWP::UserAgent;

=head1 NAME

Redmine::Simple::API::Issues - Simple interface to communicate with the API redmine

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Redmine::Simple::API;

    my $foo = Redmine::Simple::API->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 function1

=cut

has id          => (is => "rw", isa => "Int");
has project     => (is => "rw", isa => "Int");
has tracker     => (is => "rw", isa => "Int");
has status      => (is => "rw", isa => "Str");
has priority    => (is => "rw", isa => "Str");
has author      => (is => "rw", isa => "Int"); #"Maybe[Net::Redmine::User]");
has category    => (is => "rw", isa => "Int");
has subject     => (is => "rw", isa => "Str");
has description => (is => "rw", isa => "Str");
has start_date  => (is => 'rw', isa => "DateTime");
has due_date    => (is => 'rw', isa => "DateTime");
has done_ratio  => (is => 'rw', isa => "Int");
has estimated_hours => (is => 'rw', isa => 'Int');
has created_on  => (is => "rw", isa => "DateTime");
has update_on   => (is => "rw", isa => "DateTime");

#has note        => (is => "rw", isa => "Str");
#has histories   => (is => "rw", isa => "ArrayRef", lazy_build => 1);

sub get_issues {
	my $self = shift;

	my $ua = LWP::UserAgent->new;
	my $response = $ua->get($self->url . 'issues.json?key=' . $self->apikey);

	return $response->is_success ? $response->decoded_content :
	$response->status_line;
}

=head2 function2

=cut

sub function2 {
}

=head1 AUTHOR

Nelo R. Tovar, C<< <tovar.nelo at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-redmine-simple-api at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Redmine-Simple-API>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Redmine::Simple::API


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Redmine-Simple-API>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Redmine-Simple-API>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Redmine-Simple-API>

=item * Search CPAN

L<http://search.cpan.org/dist/Redmine-Simple-API/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2012 Nelo R. Tovar.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.


=cut

__PACKAGE__->meta->make_immutable;
1; # End of Redmine::Simple::API::Issues
